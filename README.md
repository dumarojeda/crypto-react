# Crypto React

Este proyecto consta de un sitio web para obtener información de cripto-monedas.

## Recursos utilizados

Las tecnologias y recursos utilizados fueron los siguientes:

Javascript
* React
* Typescript
* Testing library React

Assets
* Sass
* Bootstrap
* Cryptofonts.com

API
* Coinlore.com

## Como instalar el proyecto

* Primero debemos clonar el repositorio:
`git clone git@gitlab.com:dumarojeda/crypto-react.git`

* Luego desde la terminal se debe acceder al folder:
`cd crypto-react`

* Después se deben instalar todas las dependencias:
`npm install || yarn install`

* Ya esta la aplicación lista se puede correr sin ningún problema:
`npm start || yarn start`

* Ahora puedes entrar a [http://localhost:3000/](http://localhost:3000/)

* Tambien se pueden correr los test de la siguiente manera:
`npm test || yarn test`

Eso es todo, muchas gracias!

#### Proyecto realizado por Dumar Ojeda.
