import { useState, useEffect } from 'react';
import { ICoins } from '../interfaces/ICoins';

const GetAllCoins = (API:string) => {
  const [ coins, setCoins ] = useState<ICoins[]>([]);

  useEffect(() => {
    fetch(API)
      .then(response => response.json())
      .then(response => setCoins(response.data))
      .catch(error => console.log(error));
  }, [API]);

  return coins;
}

export default GetAllCoins;