import { useState, useEffect } from 'react';
import { ICoinDetail } from '../interfaces/ICoinDetail';

const GetCoin = (API:string) => {
  const [ coin, setCoin ] = useState<ICoinDetail[]>([]);

  useEffect(() => {
    fetch(API)
      .then(response => response.json())
      .then(response => setCoin(response))
      .catch(error => console.log(error));
  }, [API]);

  return coin;
}

export default GetCoin;