import { ICoins } from '../interfaces/ICoins';
import numberWithCommas from '../helpers/FormatNumbers';

const BoxCoin = (data: ICoins) => {
  const {id, rank, name, symbol, price_usd, percent_change_24h} = data;
  return (
    <div className="col-12 col-sm-6 col-md-4 col-lg-3" key={parseInt(id)}>
      <div className="coin-item" data-testid="coin-item">
        <div className="rank" data-testid="rank">{rank} º</div>
        <div className="head">
          <i className={`icon cf cf-${symbol.toLowerCase()}`} data-testid="icon"></i>
          <div className="name" data-testid="name">{name} <span data-testid="symbol">{symbol}</span></div>
        </div>
        <div className="details">                
          <div className="item">
            <div className="label">Precio en USD</div>
            <div className="value" data-testid="usd-item">${numberWithCommas(parseInt(price_usd))}</div>
          </div>
          <div className="item">
            <div className="label">Cambio 24H</div>
            <div className={`value ${parseInt(percent_change_24h) >= 0 ? 'green' : 'red'}`} data-testid="percent-item">{percent_change_24h}%</div>
          </div>
        </div>
        <a href={`/coin/${id}`} className='button'>Ver detalle</a>
      </div>
    </div>
  );
}

export default BoxCoin;
