import { ICoinDetail } from '../interfaces/ICoinDetail';
import numberWithCommas from '../helpers/FormatNumbers';

const Coin = (data: ICoinDetail) => {
  const {id, rank, name, symbol, price_usd, percent_change_1h, percent_change_24h, percent_change_7d, market_cap_usd, volume24, price_btc} = data;
  return (
    <div key={parseInt(id)}>
      <div className="head-section">
        <h2 className="second-title">
          <i className={`icon cf cf-${symbol.toLowerCase()}`} data-testid="icon"></i>
          <div className="name" data-testid="name">{name} <span data-testid="symbol">{symbol}</span></div>
        </h2>
        <a href="/" className="button">Regresar</a>
      </div>
      <div className="detail-coin">
        <div className="row">
          <div className="col-12 col-sm-6 col-md-4 col-lg-3">                  
            <div className="item">
              <div className="label">Rank</div>
              <div className="value" data-testid="rank">{rank} º</div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-4 col-lg-3">
            <div className="item">
              <div className="label">Precio en USD</div>
              <div className="value" data-testid="usd-item">${numberWithCommas(parseInt(price_usd))}</div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-4 col-lg-3">                  
            <div className="item">
              <div className="label">Cambio 1H</div>
              <div className={`value ${parseInt(percent_change_1h) >= 0 ? 'green' : 'red'}`} data-testid="percent-item-1h">{percent_change_1h}%</div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-4 col-lg-3">
            <div className="item">
              <div className="label">Cambio 24H</div>
              <div className={`value ${parseInt(percent_change_24h) >= 0 ? 'green' : 'red'}`} data-testid="percent-item-24h">{percent_change_24h}%</div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-4 col-lg-3">                  
            <div className="item">
              <div className="label">Cambio 7D</div>
              <div className={`value ${parseInt(percent_change_7d) >= 0 ? 'green' : 'red'}`} data-testid="percent-item-7d">{percent_change_7d}%</div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-4 col-lg-3">                  
            <div className="item">
              <div className="label">Marketcap USD</div>
              <div className='value' data-testid="market-cap-usd">${numberWithCommas(parseInt(market_cap_usd))}</div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-4 col-lg-3">                  
            <div className="item">
              <div className="label">Volumen 24H</div>
              <div className='value' data-testid="volume24">${numberWithCommas(parseInt(volume24))}</div>
            </div>
          </div>
          <div className="col-12 col-sm-6 col-md-4 col-lg-3">                  
            <div className="item">
              <div className="label">Precio en BTC</div>
              <div className='value' data-testid="price-btc">{price_btc} BTC</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Coin;
