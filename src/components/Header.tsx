const Header = () => (
  <div className="wrapper-header">
    <div className="row">
      <div className="col">
        <h1 className="main-title">Crypto React</h1>
      </div>
    </div>
  </div>
);

export default Header;
