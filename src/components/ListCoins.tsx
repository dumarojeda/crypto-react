import { useState } from 'react';
import GetAllCoins from '../api/GetAllCoins';
import Loading from './Loading';
import BoxCoin from './BoxCoin';

const API = 'https://api.coinlore.net/api/tickers/';

const ListCoins = () => {
  const coins = GetAllCoins(API);

  const [search, setSearch] = useState<string>('');

  const filteredCoins = coins.filter(coin => coin.name.toLowerCase().includes(search.toLowerCase()));

  return (
    coins.length === 0 ? <Loading /> : 
      <>
        <div className="wrapper-search">
          <div className="row justify-content-center">
            <div className="col-6">
              <input type="text" className="search-box" data-testid="search-box" placeholder="Buscar coin..." onChange={e => setSearch(e.target.value)}/>
            </div>
          </div>
        </div>
        <div className='wrapper-coins'>
          <h2 className="second-title">Lista de Coins</h2>
          <div className="row justify-content-center">
            {filteredCoins.map(coin => (
              <BoxCoin key={parseInt(coin.id)} {...coin} />
              ))}
          </div>
        </div>
      </>
  );
}

export default ListCoins;
