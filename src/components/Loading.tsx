import loaderImg from '../assets/images/loader.gif';

const Loading = () => (
  <div className="row justify-content-center">
    <div className="col-4">
      <div className="loading">
        <img src={loaderImg} className="loader" data-testid="loader" alt="loading" />
        <span className="text" data-testid="text">Cargando</span>
      </div>
    </div>
  </div>
);

export default Loading;
