import Home from '../pages/Home';
import CoinDetail from '../pages/CoinDetail';
import '../assets/styles/App.scss';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

const App = () => (
  <>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/coin/:id" element={<CoinDetail />} />
      </Routes>
    </BrowserRouter>
  </>
);

export default App;
