import { useParams } from 'react-router-dom';
import GetCoin from '../api/GetCoin';
import Loading from '../components/Loading';
import Coin from '../components/Coin';
import Header from '../components/Header';
import Footer from '../components/Footer';

const CoinDetail = () => {
  const { id } = useParams();
  const API = `https://api.coinlore.net/api/ticker/?id=${id}`;
  const coin = GetCoin(API);
  
  return (
    <div className='container'>
      <Header />
      <div className='wrapper-coins'>
        <div className="row justify-content-center">
          {coin.length === 0 ? <Loading /> : coin.map(coin => (
            <Coin key={parseInt(coin.id)} {...coin} />
            ))}
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default CoinDetail;