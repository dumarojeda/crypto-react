import Header from '../components/Header';
import Footer from '../components/Footer';
import ListCoins from '../components/ListCoins';
const Home = () => (
  <div className='container'>
    <Header />
    <ListCoins />
    <Footer />
  </div>
);

export default Home;