import { render } from '@testing-library/react';
import BoxCoin from '../components/BoxCoin';
import numberWithCommas from '../helpers/FormatNumbers';

describe('<BoxCoin/>', () => {
  const coin = {
    id: "90",
    symbol: "BTC",
    name: "Bitcoin",
    rank: 1,
    price_usd: "50308.80",
    percent_change_24h: "3.27"
  };

  const { getByTestId } = render(<BoxCoin key={parseInt(coin.id)} {...coin}/>);
  const coinItem = getByTestId('coin-item');
  const icon = getByTestId('icon');
  const name = getByTestId('name');
  const symbol = getByTestId('symbol');
  const rank = getByTestId('rank');
  const usdItem = getByTestId('usd-item');
  const percentItem = getByTestId('percent-item');

  const symbolLowerCase = coin.symbol.toLowerCase();
  const classPercent = parseInt(coin.percent_change_24h) >= 0 ? 'green' : 'red';

  test('Renders correctly elements', () => {
    expect(coinItem).toBeInTheDocument();
    expect(icon).toBeInTheDocument();
    expect(name).toBeInTheDocument();
    expect(symbol).toBeInTheDocument();
    expect(rank).toBeInTheDocument();
    expect(usdItem).toBeInTheDocument();
    expect(percentItem).toBeInTheDocument();
  });

  test('Render data in elements', () => {
    expect(name).toHaveTextContent(coin.name);
    expect(symbol).toHaveTextContent(coin.symbol);
    expect(rank).toHaveTextContent(`${coin.rank} º`);
    expect(usdItem).toHaveTextContent(`$${numberWithCommas(parseInt(coin.price_usd))}`);
    expect(percentItem).toHaveTextContent(`${coin.percent_change_24h}%`);
  });

  test('Set class for coin', () => {
    expect(icon).toHaveClass(`cf-${symbolLowerCase}`);
  });

  test('Set class state in percent 24H', () => {
    expect(percentItem).toHaveClass(classPercent);
  });
});
