import { render } from '@testing-library/react';
import Coin from '../components/Coin';
import numberWithCommas from '../helpers/FormatNumbers';

describe('<Coin/>', () => {
  const coin = {
    id: "80",
    symbol: "ETH",
    name: "Ethereum",
    nameid: "ethereum",
    rank: 2,
    price_usd: "3761.36",
    percent_change_24h: "-5.61",
    percent_change_1h: "1.56",
    percent_change_7d: "-10.39",
    market_cap_usd: "445973584545.49",
    volume24: "17303890980.09",
    volume24_native: "4600439.38",
    csupply: "118567231.00",
    price_btc: "0.080332",
    tsupply: "118567231",
    msupply: ""
  };

  const { getByTestId } = render(<Coin key={parseInt(coin.id)} {...coin}/>);
  const icon = getByTestId('icon');
  const name = getByTestId('name');
  const symbol = getByTestId('symbol');
  const rank = getByTestId('rank');
  const usdItem = getByTestId('usd-item');
  const percentItem1H = getByTestId('percent-item-1h');
  const percentItem24H = getByTestId('percent-item-24h');
  const percentItem7D = getByTestId('percent-item-7d');
  const marketCapUsd = getByTestId('market-cap-usd');
  const volume24 = getByTestId('volume24');
  const priceBtc = getByTestId('price-btc');

  const symbolLowerCase = coin.symbol.toLowerCase();
  const classPercent1H = parseInt(coin.percent_change_1h) >= 0 ? 'green' : 'red';
  const classPercent24H = parseInt(coin.percent_change_24h) >= 0 ? 'green' : 'red';
  const classPercent7D = parseInt(coin.percent_change_7d) >= 0 ? 'green' : 'red';

  test('Renders correctly elements', () => {
    expect(icon).toBeInTheDocument();
    expect(name).toBeInTheDocument();
    expect(symbol).toBeInTheDocument();
    expect(rank).toBeInTheDocument();
    expect(usdItem).toBeInTheDocument();
    expect(percentItem1H).toBeInTheDocument();
    expect(percentItem24H).toBeInTheDocument();
    expect(percentItem7D).toBeInTheDocument();
    expect(marketCapUsd).toBeInTheDocument();
    expect(volume24).toBeInTheDocument();
    expect(priceBtc).toBeInTheDocument();
  });

  test('Render data in elements', () => {
    expect(name).toHaveTextContent(coin.name);
    expect(symbol).toHaveTextContent(coin.symbol);
    expect(rank).toHaveTextContent(`${coin.rank} º`);
    expect(usdItem).toHaveTextContent(`$${numberWithCommas(parseInt(coin.price_usd))}`);
    expect(percentItem1H).toHaveTextContent(`${coin.percent_change_1h}%`);
    expect(percentItem24H).toHaveTextContent(`${coin.percent_change_24h}%`);
    expect(percentItem7D).toHaveTextContent(`${coin.percent_change_7d}%`);
    expect(marketCapUsd).toHaveTextContent(`$${numberWithCommas(parseInt(coin.market_cap_usd))}`);
    expect(volume24).toHaveTextContent(`$${numberWithCommas(parseInt(coin.volume24))}`);
    expect(priceBtc).toHaveTextContent(`${coin.price_btc} BTC`);
  });

  test('Set class for coin', () => {
    expect(icon).toHaveClass(`cf-${symbolLowerCase}`);
  });

  test('Set class state in percent items', () => {
    expect(percentItem1H).toHaveClass(classPercent1H);
    expect(percentItem24H).toHaveClass(classPercent24H);
    expect(percentItem7D).toHaveClass(classPercent7D);
  });
});
