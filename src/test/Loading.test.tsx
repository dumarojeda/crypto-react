import { render } from '@testing-library/react';
import Loading from '../components/Loading';
import loaderImg from '../assets/images/loader.gif';

describe('<Loading/>', () => {

  const { getByTestId } = render(<Loading />);

  const loader = getByTestId('loader');
  const text = getByTestId('text');

  test('Renders correctly elements', () => {
    expect(loader).toBeInTheDocument();
    expect(text).toBeInTheDocument();
  });

  test('Render data in elements', () => {
    expect(loader).toHaveAttribute('src', loaderImg);
    expect(text).toHaveTextContent('Cargando');
  });
});
